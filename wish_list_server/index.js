const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const cors = require('cors')
const multer = require('multer')
const WishListController = require('./controllers/WishListContoller')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(express.static(__dirname + '/assets'))

const storageConfig = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'assets')
    },
    filename: (req, file, cb) => {
        const mime = file.mimetype.split('/')
        const extenstion = mime[mime.length - 1]
        cb(null, `${file.originalname}_${Date.now()}.${extenstion}`)
    },
})
app.use(multer({ storage: storageConfig, dest: 'assets' }).single('file'))

app.get('/wish-list', WishListController.getLists)
app.post('/wish-list', WishListController.addElement)
app.post('/sort', WishListController.sortList)
app.put('/wish-list', WishListController.updateElement)
app.delete('/wish-list/:id', WishListController.removeElement)

app.listen(3000, () => console.log('started'))
