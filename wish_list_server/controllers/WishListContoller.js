const fs = require('fs')
module.exports = {
    getLists: async (req, res) => {
        try {
            const DB = JSON.parse(fs.readFileSync('./db.json'))
            await new Promise((r) => setTimeout(r, 100))
            return res.json([...DB.sort((a, b) => a.order - b.order)])
        } catch (e) {
            res.status(500).json({ msg: e.message })
        }
    },
    addElement: async (req, res) => {
        if (!req.body) return res.sendStatus(400)

        try {
            const filedata = req.file
            const { body } = req
            const DB = JSON.parse(fs.readFileSync('./db.json'))
            const list = [...DB]
            list.push({
                ...body,
                img: filedata
                    ? `${req.protocol}://${req.get('host')}/${
                          filedata.filename
                      }`
                    : body.img,
                isPriority: body.isPriority === 'true' ? true : false,
            })
            const bodyJson = JSON.stringify(list)
            await new Promise((r) => setTimeout(r, 100))
            fs.writeFile('./db.json', bodyJson, function (err) {
                if (err) throw err
                res.sendStatus(201)
            })
        } catch (e) {
            res.status(500).json({ msg: e.message })
        }
    },
    updateElement: async (req, res) => {
        if (!req.body) return res.sendStatus(400)

        try {
            const filedata = req.file
            const { body } = req
            const DB = JSON.parse(fs.readFileSync('./db.json'))
            let list = [...DB]
            const item = DB.find((elem) => elem.id === body.id)
            if (item) {
                list = DB.map((elem) => {
                    if (elem.id === item.id)
                        return {
                            ...body,
                            img: filedata
                                ? `${req.protocol}://${req.get('host')}/${
                                      filedata.filename
                                  }`
                                : !body.img ? '' : elem.img,
                            isPriority:
                                body.isPriority === 'true' ? true : false,
                        }
                    return elem
                })
            } else {
                list.push({
                    ...body,
                    img: filedata
                        ? `${req.protocol}://${req.get('host')}/${
                              filedata.filename
                          }`
                        : body.img,
                    isPriority: body.isPriority === 'true' ? true : false,
                })
            }
            const bodyJson = JSON.stringify(list)
            await new Promise((r) => setTimeout(r, 100))
            fs.writeFile('./db.json', bodyJson, function (err) {
                if (err) throw err
                res.sendStatus(201)
            })
        } catch (e) {
            res.status(500).json({ msg: e.message })
        }
    },
    removeElement: async (req, res) => {
        const { id } = req.params

        try {
            const DB = JSON.parse(fs.readFileSync('./db.json'))
            const list = DB.filter((elem) => elem.id !== id)

            const bodyJson = JSON.stringify(list)
            await new Promise((r) => setTimeout(r, 100))
            fs.writeFile('./db.json', bodyJson, function (err) {
                if (err) throw err
                res.status(201).json(list)
            })
        } catch (e) {
            res.status(500).json({ msg: e.message })
        }
    },
    sortList: async (req, res) => {
        if (!req.body) return res.sendStatus(400)

        try {
            const { body } = req

            const DB = JSON.parse(fs.readFileSync('./db.json'))

            const list = DB.map((item) => {
                const bodyItem = body.find((b) => b.id === item.id)
                if (bodyItem)
                    return {
                        ...item,
                        order: bodyItem.order,
                    }
                return item
            })
            const bodyJson = JSON.stringify(list)
            fs.writeFile('./db.json', bodyJson, function (err) {
                if (err) throw err
                res.sendStatus(201)
            })
        } catch (e) {
            res.status(500).json({ msg: e.message })
        }
    },
}
