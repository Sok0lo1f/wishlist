import { useContext, useEffect, useState } from 'react'
import './App.scss'
import { ListItemEditor } from './components/ListItemEditor/ListItemEditor'
import { List } from './components/List/List'
import { Modal } from './components/Modal/Modal'
import { useModal } from './components/Modal/useModal'
import useAxios from 'axios-hooks'
import { PATHS } from './utils/apiPaths'
import { ListContext } from './store/store'
import { Types } from './store/types'
import { Loader } from './components/Loader/Loader'
const App = () => {
    const { isShow, toggle } = useModal()
    const { state, dispatch } = useContext(ListContext)
    const [{ data, loading, error }, refresh] = useAxios(
        `${PATHS.URL}${PATHS.WISH_LIST}`
    )
    useEffect(() => {
        if (!loading && data) {
            dispatch({
                type: Types.SET_LIST,
                payload: data,
            })
        }
    }, [data])
    useEffect(() => {
        if (error) alert('Неудалось загрузить список')
    }, [error])
    return (
        <div className='main-page'>
            {loading ? (
                <div className='main-page__loader'>
                    <Loader />
                </div>
            ) : (
                <>
                    <header className='header'>
                        <h1 className='header__title'>My Wish List</h1>
                        <button className='header__btn-add' onClick={toggle}>
                            Добавить
                        </button>
                    </header>

                    <List />
                    <Modal isShow={isShow} hide={toggle}>
                        <ListItemEditor close={toggle} />
                    </Modal>
                </>
            )}
        </div>
    )
}

export default App
