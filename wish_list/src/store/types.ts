import { IList } from "../interfaces/list"
export enum ListSortTypes {
    NAME_DESC = 'NAME_DESC',
    NAME_ASC = 'NAME_ASC',
    NONE = 'NONE'
}
export enum Types {
    ADD = 'ADD',
    REMOVE = 'REMOVE',
    EDIT = 'EDIT',
    SET_LIST = 'SET_LIST',
    SET_SORT_TYPE = 'SET_SORT_TYPE'

}

interface ActionAddListElem {
    type: Types.ADD
    payload: IList
}
interface ActionRemoveListElem {
    type: Types.REMOVE
    payload: string
}
interface ActionEditListElem {
    type: Types.EDIT
    payload: IList
}
interface ActionSetList {
    type: Types.SET_LIST
    payload: IList[]
}
interface ActionSetSortType {
    type: Types.SET_SORT_TYPE,
    payload: {
        sortType: ListSortTypes
        list: IList[]
    }
}
export type ActionType = ActionAddListElem 
                        | ActionRemoveListElem 
                        | ActionEditListElem 
                        | ActionSetList 
                        | ActionSetSortType