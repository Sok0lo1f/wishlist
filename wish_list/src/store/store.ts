import { createContext, Dispatch } from 'react'
import { IList } from '../interfaces/list'
import { sortList } from '../utils/sortList'
import { ActionType, ListSortTypes, Types } from './types'


interface ListState {
    wishList: IList[]
    sortType: ListSortTypes
}
export const initState: ListState = {
    wishList: [],
    sortType: localStorage.getItem('sortType') as ListSortTypes || ListSortTypes.NONE
}

export const ListContext = createContext<{
    state: ListState, 
    dispatch: Dispatch<ActionType>
}>({state: initState, dispatch: () => null})

export const ListReducer = (state: ListState, action: ActionType): ListState => {
    switch (action.type) {
        case Types.ADD:
           return {
                ...state,
                wishList: [...state.wishList, {
                    ...action.payload,
                    order: state.wishList.length
                }],
                // sortType: ListSortTypes.NONE
            }
        case Types.REMOVE:
            return {
                ...state,
                wishList: state.wishList.filter(item => item.id !== action.payload),
            }
        case Types.EDIT:
            return {
                ...state,
                wishList: state.wishList.map(item => {     
                    if(item.id === action.payload.id) return action.payload
                    return item
                }),
                // sortType: ListSortTypes.NONE
            }  
        case Types.SET_LIST:
            return {
                ...state,
                wishList: action.payload,
                // sortType: ListSortTypes.NONE
            }
        case Types.SET_SORT_TYPE: 
            const newList = sortList(action.payload.list, action.payload.sortType)
            return {
                ...state,
                sortType: action.payload.sortType,
                wishList: newList
            }
        default:
            return state
    }
}