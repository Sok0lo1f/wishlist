import React, { useReducer } from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
import { initState, ListContext, ListReducer } from './store/store'

const Main = () => {
    const [state, dispatch] = useReducer(ListReducer, initState)
    return (
        <React.StrictMode>
            <ListContext.Provider value={{ state, dispatch }}>
                <App />
            </ListContext.Provider>
        </React.StrictMode>
    )
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <Main />
)
