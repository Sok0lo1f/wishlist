import { IList } from '../interfaces/list'
import { ListSortTypes } from '../store/types'

export const sortList = (list: IList[], type: ListSortTypes) => {
    const newList = [...list]
    switch(type) {
        case ListSortTypes.NAME_ASC:
            return newList.sort((a, b) => a.name.localeCompare(b.name))
        case ListSortTypes.NAME_DESC:
            return newList.sort((a, b) => b.name.localeCompare(a.name))
        case ListSortTypes.NONE:
            return newList
    }
}