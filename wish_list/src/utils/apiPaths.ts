export enum PATHS {
    URL = 'http://localhost:3000/',
    WISH_LIST = 'wish-list',
    SORT = 'sort'
}