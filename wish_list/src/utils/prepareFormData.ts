import { IList } from '../interfaces/list'

export const prepareFormData = (elem: IList, file: File | null) => {
    const formData = new FormData()
    if (file) {
        formData.append('file', file)
    }
    if (!file && elem.img)
        formData.append('img', elem.img)
    formData.append('id', elem.id)
    formData.append('name', elem.name.trim())
    formData.append('isPriority', elem.isPriority ? 'true' : 'false')


    return formData
}