import React, { ChangeEvent, FC, useContext, useEffect, useRef } from 'react'
import { ListContext } from '../../store/store'
import { ListSortTypes, Types } from '../../store/types'
import './Filters.scss'
const sortingList = [
    {
        id: ListSortTypes.NONE,
        name: '--',
        isHidden: true,
    },
    {
        id: ListSortTypes.NAME_ASC,
        name: 'Имени (по возр.)',
        isHidden: false,
    },
    {
        id: ListSortTypes.NAME_DESC,
        name: 'Имени (по уб.)',
        isHidden: false,
    },
]
interface FiltersProps {
    onlyFavorites: boolean
    onChange: (isOnlyFav: boolean) => void
}
export const Filters: FC<FiltersProps> = ({ onChange, onlyFavorites }) => {
    const { state, dispatch } = useContext(ListContext)
    const selectRef = useRef<HTMLSelectElement>(null)

    useEffect(() => {
        localStorage.setItem('sortType', state.sortType)
        if (selectRef.current) selectRef.current.value = state.sortType
    }, [state.sortType])
    const onSortChange = (e: ChangeEvent<HTMLSelectElement>) => {
        const type = e.target.value
        dispatch({
            type: Types.SET_SORT_TYPE,
            payload: {
                sortType: type as ListSortTypes,
                list: state.wishList,
            },
        })
    }
    return (
        <div className='wish-list__filters'>
            <div className='wish-list__filters-sort'>
                <label>Отсортировать по:</label>
                <select
                    ref={selectRef}
                    className='wish-list__filters-sort--select'
                    onChange={onSortChange}
                    defaultValue={state.sortType}>
                    {sortingList.map((sortElem) => (
                        <option
                            value={sortElem.id}
                            hidden={sortElem.isHidden}
                            disabled={sortElem.isHidden}
                            key={sortElem.id}>
                            {sortElem.name}
                        </option>
                    ))}
                </select>
            </div>
            <div className='wish-list__filters-only-favorites'>
                <input
                    type='checkbox'
                    className='custom-checkbox'
                    id='only_fav'
                    name='only_fav'
                    checked={onlyFavorites}
                    value={`${onlyFavorites}`}
                    onChange={(e) => onChange(e.target.checked)}
                />
                <label htmlFor='only_fav'>Показывать только избранные</label>
            </div>
        </div>
    )
}
