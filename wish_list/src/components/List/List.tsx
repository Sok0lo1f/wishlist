import useAxios from 'axios-hooks'
import React, {
    FC,
    useContext,
    useEffect,
    useMemo,
    useRef,
    useState,
} from 'react'
import { ListContext } from '../../store/store'
import { ListSortTypes, Types } from '../../store/types'
import { PATHS } from '../../utils/apiPaths'
import { Filters } from '../Filters/Filters'
import { ListItem } from '../ListItem/ListItem'
import './List.scss'

export const List: FC = () => {
    const { state, dispatch } = useContext(ListContext)

    const [onlyFav, setOnlyFav] = useState(false)

    const [isDragging, setDragging] = useState(false)
    const dragItem = useRef<number | null>(null)
    const dragNode = useRef<EventTarget | null>()

    const handleDragStart = (
        e: React.DragEvent<HTMLDivElement>,
        index: number
    ) => {
        dragItem.current = index
        dragNode.current = e.target
        dragNode.current.addEventListener('dragend', handleDragEnd)
        setTimeout(() => {
            setDragging(true)
        }, 0)
    }
    const handleDragEnter = (
        e: React.DragEvent<HTMLDivElement>,
        index: number
    ) => {
        const currentItem = dragItem.current
        if (e.target !== dragNode.current) {
            const newList = [...state.wishList]
            newList.splice(
                index,
                0,
                newList.splice(currentItem as number, 1)[0]
            )
            dragItem.current = index

            dispatch({
                type: Types.SET_SORT_TYPE,
                payload: {
                    list: newList.map((item, i) => ({
                        ...item,
                        order: i,
                    })),
                    sortType: ListSortTypes.NONE,
                },
            })
        }
    }

    const [data, sendSorted] = useAxios(
        {
            url: `${PATHS.URL}${PATHS.SORT}`,
            method: 'POST',
        },
        { manual: true }
    )
    const handleDragEnd = (e: Event) => {
        setDragging(false)
        dragNode.current?.removeEventListener('dragend', handleDragEnd)
        dragItem.current = null
        dragNode.current = null
    }

    useEffect(() => {
        let timeout: number
        if (state.wishList.length > 0)
            timeout = setTimeout(() => {
                sendSorted({
                    data: state.wishList.map((el, i) => ({
                        id: el.id,
                        order: i,
                    })),
                })
            }, 500)

        return () => {
            clearTimeout(timeout)
        }
    }, [state.wishList])

    const getStyles = (index: number) => {
        const currentItem = dragItem.current
        return currentItem === index
            ? 'wish-list__item --current'
            : 'wish-list__item'
    }

    const filteredList = useMemo(() => {
        return state.wishList.filter((item) => {
            if (onlyFav) return item.isPriority
            return true
        })
    }, [state.wishList, onlyFav])
    return (
        <div className='wish-list'>
            <Filters onlyFavorites={onlyFav} onChange={setOnlyFav} />
            {filteredList.length > 0 ? (
                filteredList.map((item, index) => (
                    <div
                        key={JSON.stringify(item)} //Пришлось использовать так, так как при key=item.id работало некорректно при удалении изображения
                        // key={item.id}
                        draggable={!onlyFav}
                        onDragOver={(e) => e.preventDefault()}
                        onDragEnter={
                            isDragging
                                ? (e) => handleDragEnter(e, index)
                                : () => {}
                        }
                        className={
                            isDragging ? getStyles(index) : 'wish-list__item'
                        }
                        onDragStart={(e) => handleDragStart(e, index)}>
                        <ListItem
                            listElement={item}
                            index={index}
                            isOnlyFav={onlyFav}
                        />
                    </div>
                ))
            ) : (
                <div className='wish-list__empty'>Список пуст :(</div>
            )}
        </div>
    )
}
