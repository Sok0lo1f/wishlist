import React, { ChangeEvent, FC, useContext, useEffect, useState } from 'react'
import { IList } from '../../interfaces/list'
import { ListContext } from '../../store/store'
import { Types } from '../../store/types'
import { v4 } from 'uuid'
import './ListItemEditor.scss'
import { UploadFile } from '../UploadFile/UploadFile'
import useAxios from 'axios-hooks'
import { PATHS } from '../../utils/apiPaths'
import { prepareFormData } from '../../utils/prepareFormData'
import { Loader } from '../Loader/Loader'

interface ListItemEditor {
    listItemId?: string
    isAdd?: boolean
    close: () => void
}
export const ListItemEditor: FC<ListItemEditor> = ({
    isAdd = true,
    listItemId,
    close,
}) => {
    const { state, dispatch } = useContext(ListContext)
    const [listElement, setElement] = useState<IList>({
        id: v4(),
        name: '',
        isPriority: false,
        order: 0,
        img: '',
    })
    const [_file, setFile] = useState<File | null>(null)
    useEffect(() => {
        if (!isAdd && listItemId) {
            const item = state.wishList.find((elem) => elem.id === listItemId)
            if (item) setElement(item)
            else {
                alert('Произошла ошибка при получении элемента')
                close()
            }
        }
    }, [])

    //QUERIES
    const [
        { data: postData, loading: postLoading, error: postError },
        sendData,
    ] = useAxios(
        {
            url: `${PATHS.URL}${PATHS.WISH_LIST}`,
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        },
        { manual: true }
    )
    const [
        { data: putData, loading: putLoading, error: putError },
        updateData,
    ] = useAxios(
        {
            url: `${PATHS.URL}${PATHS.WISH_LIST}`,
            method: 'PUT',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        },
        { manual: true }
    )

    const onNameChange = (e: ChangeEvent<HTMLInputElement>) => {
        setElement({
            ...listElement,
            name: e.target.value,
        })
    }

    const onSubmit = async () => {
        const formData = prepareFormData(listElement, _file)

        if (isAdd) {
            sendData({
                data: formData,
            })
        } else {
            updateData({
                data: formData,
            })
        }
    }

    useEffect(() => {
        if ((postData && !postLoading) || (putData && !putLoading)) {
            dispatch({
                type: isAdd ? Types.ADD : Types.EDIT,
                payload: {
                    ...listElement,
                    name: listElement.name.trim(),
                },
            })
            close()
        }
    }, [postData, postLoading, putData, putLoading])

    const onFileChange = (file: File | null, src: string | null) => {
        setElement((prev) => ({
            ...prev,
            img: src,
        }))
        setFile(file)
    }

    return (
        <div className='list-item-editor'>
            <h2 className='list-item-editor__header'>
                {isAdd ? 'Добавление' : 'Редактирование'}
            </h2>
            <div className='list-item-editor__inputs'>
                <label htmlFor='name'>Название</label>
                <input
                    name='name'
                    value={listElement.name}
                    onChange={onNameChange}
                />
            </div>
            <UploadFile
                onChange={onFileChange}
                defaultValue={listElement.img || ''}
            />

            <p>{postError?.message}</p>
            <button
                className='list-item-editor__submit'
                disabled={
                    listElement.name.trim().length === 0 ||
                    putLoading ||
                    postLoading
                }
                onClick={onSubmit}>
                {putLoading || postLoading ? (
                    <div className='btn-loader'>
                        <Loader />
                    </div>
                ) : (
                    'Сохранить'
                )}
            </button>
        </div>
    )
}
