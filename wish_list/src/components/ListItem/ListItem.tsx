import React, { FC, useContext, useEffect } from 'react'
import { IList } from '../../interfaces/list'
import { ListContext } from '../../store/store'
import { ListSortTypes, Types } from '../../store/types'
import { ListItemEditor } from '../ListItemEditor/ListItemEditor'
import { Modal } from '../Modal/Modal'
import { useModal } from '../Modal/useModal'
import classNames from 'classnames'
import Favorite from '../../assets/favorite.png'
import FavoriteGold from '../../assets/favoriteGold.png'
import IconEdit from '../../assets/iconEdit.png'
import Remove from '../../assets/trash.png'
import Placeholder from '../../assets/placeholder.jpeg'
import './ListItem.scss'
import { useImage } from '../../hooks/useImage'
import useAxios from 'axios-hooks'
import { PATHS } from '../../utils/apiPaths'
import { Loader } from '../Loader/Loader'
import { prepareFormData } from '../../utils/prepareFormData'
import Arrow from '../../assets/arrow.svg'

interface ListItemProps {
    listElement: IList
    index: number
    isOnlyFav: boolean
}
export const ListItem: FC<ListItemProps> = ({
    listElement,
    index,
    isOnlyFav,
}) => {
    const { state, dispatch } = useContext(ListContext)
    const { isShow, toggle } = useModal()

    const [
        { data: newList, loading: deleteLoading, error: deleteError },
        removeItem,
    ] = useAxios<IList[]>(
        {
            url: `${PATHS.URL}${PATHS.WISH_LIST}/${listElement.id}`,
            method: 'DELETE',
        },
        { manual: true }
    )
    const [
        { data: putData, loading: putLoading, error: putError },
        updateData,
    ] = useAxios(
        {
            url: `${PATHS.URL}${PATHS.WISH_LIST}`,
            method: 'PUT',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        },
        { manual: true }
    )

    useEffect(() => {
        if (newList && !deleteLoading)
            dispatch({
                type: Types.SET_LIST,
                payload: newList,
            })
    }, [newList])

    const setPriority = () => {
        const newElem = {
            ...listElement,
            isPriority: !listElement.isPriority,
        }
        const formData = prepareFormData(newElem, null)
        updateData({
            data: formData,
        })
    }

    useEffect(() => {
        if (!putLoading && putData) {
            dispatch({
                type: Types.EDIT,
                payload: {
                    ...listElement,
                    isPriority: !listElement.isPriority,
                },
            })
        }
    }, [putLoading])

    const onChangeOrder = (newOrder: number) => {
        const newList = [...state.wishList]
        newList.splice(newOrder, 0, newList.splice(index, 1)[0])
        dispatch({
            type: Types.SET_SORT_TYPE,
            payload: {
                list: newList.map((item, i) => ({
                    ...item,
                    order: i,
                })),
                sortType: ListSortTypes.NONE,
            },
        })
    }

    const listItemStyle = classNames({
        'list-item': true,
        '--priority': listElement.isPriority,
        '--loading': deleteLoading || putLoading,
        '--graggable': !isOnlyFav,
    })

    const { imgSrc } = useImage(listElement.img || '', Placeholder)

    return (
        <>
            <div className={listItemStyle}>
                {(deleteLoading || putLoading) && (
                    <div className='list-item-loader'>
                        <Loader />
                    </div>
                )}
                <div className='list-item__order'>
                    {!isOnlyFav && (
                        <button
                            className='list-item__order--btn btn-up'
                            disabled={index === 0}
                            onClick={() => onChangeOrder(index - 1)}>
                            <img src={Arrow} alt='arrow_top' />
                        </button>
                    )}
                    <span>{index + 1}</span>
                    {!isOnlyFav && (
                        <button
                            className='list-item__order--btn btn-down'
                            disabled={index === state.wishList.length - 1}
                            onClick={() => onChangeOrder(index + 1)}>
                            <img src={Arrow} alt='arrow_down' />
                        </button>
                    )}
                </div>
                <div className='list-item__img'>
                    <img src={imgSrc} alt={listElement.name} />
                </div>
                <div className='list-item__name'>{listElement.name}</div>
                <div className='list-item__btns'>
                    <button
                        className='list-item__btn favorite'
                        onClick={setPriority}>
                        <img
                            src={
                                listElement.isPriority ? FavoriteGold : Favorite
                            }
                            alt='Избранное'
                        />
                    </button>
                    <button className='list-item__btn edit' onClick={toggle}>
                        <img src={IconEdit} alt='Изменить' />
                    </button>
                    <button
                        className='list-item__btn remove'
                        onClick={() => removeItem()}>
                        <img src={Remove} alt='Удалить' />
                    </button>
                </div>
            </div>
            <Modal isShow={isShow} hide={toggle}>
                <ListItemEditor
                    isAdd={false}
                    listItemId={listElement.id}
                    close={toggle}
                />
            </Modal>
        </>
    )
}
