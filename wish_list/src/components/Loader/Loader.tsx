import React from 'react'
import './Loader.scss'
export const Loader = ({ className = '' }) => {
    return (
        <div className={`lds-ring ${className}`}>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    )
}
