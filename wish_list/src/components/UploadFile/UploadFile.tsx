import React, { ChangeEvent, FC, useEffect, useState } from 'react'
import './UploadFile.scss'

interface UploadFileProps {
    onChange: (file: File | null, src: string) => void
    defaultValue?: string
}

const MAX_FILE_SIZE = 2_097_152
export const UploadFile: FC<UploadFileProps> = ({ onChange, defaultValue }) => {
    const [_file, setFile] = useState('')
    const [error, setError] = useState('')

    const onFileLoad = (e: ChangeEvent<HTMLInputElement>) => {
        setError('')
        setFile('')
        const file = e.target.files![0]
        if (file.size > MAX_FILE_SIZE)
            return setError('Размер файла превышает 2МБ')

        const fileReader = new FileReader()
        fileReader.readAsDataURL(file)
        fileReader.onload = (e) => {
            setFile(fileReader.result as string)
            onChange(file, fileReader.result as string)
        }
        fileReader.onerror = (e) => {
            setError('Произошла ошибка при загрузке файла')
        }
    }
    useEffect(() => {
        setFile(defaultValue || '')
    }, [defaultValue])
    const removeFile = () => {
        setFile('')
        onChange(null, '')
    }
    return (
        <div className='upload-file'>
            {_file && (
                <div className='upload-file__preview'>
                    <div
                        className='upload-file__preview--remove'
                        onClick={removeFile}></div>
                    <img src={_file} alt='File' />
                </div>
            )}
            <div className='upload-file__input'>
                {!_file && (
                    <label htmlFor='upload-img'>Загрузить изображение</label>
                )}
                {error && <p>{error}</p>}
                <input
                    id='upload-img'
                    name='upload-img'
                    type={'file'}
                    accept='image/*'
                    onChange={onFileLoad}
                />
            </div>
        </div>
    )
}
