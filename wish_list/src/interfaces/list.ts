export interface IList {
    id: string
    name: string
    isPriority: boolean
    img: string | null
    order: number
}